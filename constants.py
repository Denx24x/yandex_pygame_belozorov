﻿BASE_BLOCK_SIZE = 64
BASE_BLOCK_WIDTH = 12
BASE_BLOCK_HEIGHT = 12

fps = 30

# состояния игры
IN_GAME = 0
INVENTORY = 1
PAUSE = 2
SETTINGS = 3
MENU = 4
CONNECT = 5
CONNECT_ERROR = 6

# дада персонаж с ускорением падает
GRAV_CONST = (2 * 9.8) / fps ** 2

# пределы мира
MAX_HEIGHT = -128
MIN_HEIGHT = 128

# виды запросов сервера
JOIN_REQUEST = 1
LEAVE_REQUEST = 2
BLOCK_CHANGE = 3
PING = 4
PLACE_BLOCK = 5
DESTROY_BLOCK = 6
BLOCK_UPDATE = 8
JOIN = 9
LEAVE = 10
SYNC_BLOCK = 11
SYNC_INVENTORY = 12

PING_TIME = 0.1

dirs = [(0, -1), (0, 1), (1, 0), (-1, 0)]
